# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.1.0](https://gitlab.com/qvartz-public/nurkit-test-2/compare/@qvartz/test@1.0.0...@qvartz/test@1.1.0) (2021-06-04)


### Features

* **test:** add new feature ([b996317](https://gitlab.com/qvartz-public/nurkit-test-2/commit/b996317a2faeb8964cba423448f5fd0069b34322))





# 1.0.0 (2021-06-02)


### Features

* **test:** initial commit ([f80d313](https://gitlab.com/qvartz-public/nurkit-test-2/commit/f80d313ce4024bbda9c0539f881a06250333a2df))
