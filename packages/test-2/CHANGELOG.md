# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://gitlab.com/qvartz-public/nurkit-test-2/compare/@qvartz/test-2@1.0.0...@qvartz/test-2@1.0.1) (2021-06-04)


### Bug Fixes

* **test-2:** add new fix ([83fad03](https://gitlab.com/qvartz-public/nurkit-test-2/commit/83fad03f6a92ab98e74aba59fcde585ab08922e2))





# 1.0.0 (2021-06-04)


### Features

* **test-2:** add new package ([2ec09a9](https://gitlab.com/qvartz-public/nurkit-test-2/commit/2ec09a9b7bfd749d5fb011964e76eeb13ee37e5d))
